<?php 

$pledgers = query_posts( array( 'post_type' => array('pledgers'), 'posts_per_page' => -1 ) );
// echo '<xmp>';print_r($pledgers);die;

$locations = array();
$cities = array();
$x = 0;
foreach( $pledgers as $pledge ) {
	// echo get_field('latitude', $pledge->ID);

  $date = new DateTime($pledge->post_date);
	if( floatval(get_field('latitude', $pledge->ID)) != 0 ) {
		$locations[$x]['lat'] = floatval(get_field('latitude', $pledge->ID));
		$locations[$x]['lng'] = floatval(get_field('longitude', $pledge->ID));
    $locations[$x]['name'] = $pledge->post_title;
    $locations[$x]['date'] = $date->format('M j, Y');
    // echo get_field('image', $pledge->ID) . '<br>';
    $locations[$x]['image'] = get_field('image', $pledge->ID);
		$cities[$x] = get_field('city', $pledge->ID);
		$x++;
	}
}
$counts = array_count_values($cities);
arsort($counts);
$leaders = array();
$leaders['city'] = key($counts);
$leaders['count'] = $counts[$leaders['city']];
$newLeaders = json_encode($leaders);
$newLocations = json_encode($locations);
// echo '<xmp>';print_r($newLocations);die;
// die;
?>


<?php
get_header(); 
nectar_page_header($post->ID); 

//full page
$fp_options = nectar_get_full_page_options();
extract($fp_options);

?>

<div class="container-wrap">
	
	<div class="<?php if($page_full_screen_rows != 'on') echo 'container'; ?> main-content">
		
		<div class="row">
			
			<?php 

			//breadcrumbs
			if ( function_exists( 'yoast_breadcrumb' ) && !is_home() && !is_front_page() ){ yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } 

			 //buddypress
			 global $bp; 
			 if($bp && !bp_is_blog_page()) echo '<h1>' . get_the_title() . '</h1>';
			
			 //fullscreen rows
			 if($page_full_screen_rows == 'on') echo '<div id="nectar_fullscreen_rows" data-animation="'.$page_full_screen_rows_animation.'" data-row-bg-animation="'.$page_full_screen_rows_bg_img_animation.'" data-animation-speed="'.$page_full_screen_rows_animation_speed.'" data-content-overflow="'.$page_full_screen_rows_content_overflow.'" data-mobile-disable="'.$page_full_screen_rows_mobile_disable.'" data-dot-navigation="'.$page_full_screen_rows_dot_navigation.'" data-footer="'.$page_full_screen_rows_footer.'" data-anchors="'.$page_full_screen_rows_anchors.'">';

				 if(have_posts()) : while(have_posts()) : the_post(); 
					
					 the_content(); 
				 endwhile; endif; 
				
			if($page_full_screen_rows == 'on') echo '</div>'; ?>

		</div><!--/row-->
		
	</div><!--/container-->
	
</div><!--/container-wrap-->

    <div class="campaign-modal-overlay"></div>
	    <div class="campaign-modal">
	    	<div class="campaign-header">
	    		<h3><?php echo __('otorcycleSafetyPledge', 'mcc-maps');?></h3>
		    	<h2><?php echo __('Take the Motorcycle Safety Pledge', 'mcc-maps');?></h2>
		    	<p><?php echo __('Read the pledge aloud. Say it with a friend or family member.<br>
          Most importantly find a way to make it meaningful for you!', 'mcc-maps');?></p>
		    </div>
		    <form class="campaign-form" id="campaign-form">
		    	<input type="file" id="cloudinary-input" class="sr-only">
          <input type="hidden" id="image" name="image">
          <input type="hidden" id="image_name" name="image_name">
		      <label class="type-container" for="motorcyclist">
		        <input type="checkbox" id="motorcyclist" name="pledge_type" value="motorcyclist" checked>
		        <div class="pledge-type">
		        	<h4><?php echo __('As a Motorcyclist', 'mcc-maps');?></h4>
		        	<p><?php echo __('I pledge to make safety a priority, to ride only when alert and fully prepared for the responsibility of motorcycling. I pledge to ride within my limits, ride sober, obey traffic laws and make arriving alive my greatest priority.', 'mcc-maps');?></p>
		        </div>
		      </label>
		      <label class="type-container" for="passenger">
		        <input type="checkbox" id="passenger" name="pledge_type" value="passenger">
		        <div class="pledge-type">
		        	<h4><?php echo __('As a Passenger', 'mcc-maps');?></h4>
		        	<p><?php echo __(' pledge to ride only with riders who place my safety first, who provide me with the proper riding gear, and who always ride sober.', 'mcc-maps');?>I</p>
		        </div>
		      </label>
		      <label class="type-container" for="motorist">
		        <input type="checkbox" id="motorist" name="pledge_type" value="motorist">
		        <div class="pledge-type">
		        	<h4><?php echo __('As a Motorist', 'mcc-maps');?></h4>
		        	<p><?php echo __('I pledge to treat motorcyclists with respect by always looking twice, using turn signals, checking my blind spots before switching lanes and eliminating distractions while driving. I pledge to act as if a loved one is riding on each and every motorcycle I see. I promise to drive like their life depends on me, because ultimately, it may.', 'mcc-maps');?></p>
		        </div>
		      </label>
		      <label class="type-container" for="friend">
		        <input type="checkbox" id="friend" name="pledge_type" value="friend">
		        <div class="pledge-type">
		        	<h4><?php echo __('As a Friend and Family Member', 'mcc-maps');?></h4>
		        	<p><?php echo __('I pledge to support my loved one’s enjoyment and pursuit of motorcycling. I pledge to encourage them to seek certified riding instruction, to wear the proper protective gear and to be a safe rider.', 'mcc-maps');?></p>
		        </div>
		      </label>
		      <div class="input-container">
		      	<label for="name" class="sr-only"><?php echo __('Name', 'mcc-maps');?></label>
		      	<input type="text" id="name" name="name" placeholder="<?php echo __('Name*', 'mcc-maps');?>">
		      </div>
		      <div class="input-container">
		      	<label for="last_name" class="sr-only"><?php echo __('Last Name(optional)', 'mcc-maps');?></label>
		      	<input type="text" id="last_name" name="last_name" placeholder="<?php echo __('Last Name (optional)', 'mcc-maps');?>">
		      </div>
		      <div class="input-container">
		      	<label for="city" class="sr-only"><?php echo __('City', 'mcc-maps');?></label>
		      	<input type="text" id="city" name="city" placeholder="<?php echo __('City*', 'mcc-maps');?>">
		      </div>
		      <div class="input-container">
		      	<label for="postal_code" class="sr-only"><?php echo __('Postal Code', 'mcc-maps');?></label>
		      	<input type="text" id="postal_code" name="postal_code" placeholder="<?php echo __('Postal Code*', 'mcc-maps');?>">
		      </div>
		      <div class="input-container-full clearfix">
		      	<label for="email" class="sr-only"><?php echo __('Email', 'mcc-maps');?></label>
		      	<input type="text" id="email" name="email" placeholder="<?php echo __('Email Address', 'mcc-maps');?>">
		      </div>
		      <div class="input-container-full clearfix">
		      	<input type="button" id="take-photo" name="take-photo" value="<?php echo __('Add a Photo', 'mcc-maps');?>">
		      	<input type="button" id="remove-photo" name="take-photo" value="<?php echo __('Remove Photo', 'mcc-maps');?>">
		      </div>
		      <div class="input-container-full clearfix">
			      <img src="" id="photo-taken">
			    </div>
		      <div class="input-container-full clearfix">
		      	<label for="email-confirm" class="type-container">
		      		<input type="checkbox" id="email-confirm" name="email-confirm">
		      		<?php echo __('Subscribe to the Motorcycling.ca newsletter - don\'t worry we don\'t spam! we\'ll never give away your soul.', 'mcc-maps');?>
		      		
		      	</label>
		      </div>
		      <div>
		      	<input type="hidden" name="latitude" id="latitude">
		      	<input type="hidden" name="longitude" id="longitude">
		      </div>
		      <div class="errors">
		      	<h5><?php echo __('Please correct the following errors', 'mcc-maps');?></h5>
		      </div>
		      <input id="submit-campaign" type="submit" value="Take the Safety Pledge">
		    </form>
		  </div>
			<script>
				newLocations = <?php echo $newLocations ?>;
				var leaders = <?php echo $newLeaders ?>;

			</script>
    </div>

<?php get_footer(); ?>
<!-- <script src="/wp-content/themes/mcc/js/safety-campaign.js"></script> -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd8iDLG0XI6iupgAXUD-P_QRHqy8IZGSs&callback=initMap"></script>
<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>  

<script type="text/javascript">  
	var deleteToken
  document.getElementById("take-photo").addEventListener("click", function() {
    cloudinary.openUploadWidget({ cloud_name: 'musemarketing', upload_preset: 'mcc_overlay2', sources: ['local'], multiple: false, cropping: 'server', cropping_aspect_ratio: 1.9, resource_type: 'image'}, 
      function(error, result) { 
      	if( typeof result != 'undefined') {
          console.log(result)
	      	var url = result[0].secure_url;
	      	jQuery("#photo-taken").attr('src', url)
	      	deleteToken = result[0].delete_token
	      	var hash = result[0].path.replace('/','__').replace('.jpg','')
  				jQuery('#take-photo').val('Change Photo')
          image = url
  				jQuery('#image').val(url)
          jQuery('#image_name').val(result[0].original_filename + '.' + result[0].format)
  				jQuery('#remove-photo').css('display', 'block')
  			}
      });
  }, false);

  jQuery(document).on('click', '#take-photo', function(){
  	if( typeof deleteToken != 'undefined' ) {
  		jQuery.ajax({
  			type: 'POST',
  			url: 'https://api.cloudinary.com/v1_1/musemarketing/delete_by_token',
  			data: 'token=' + deleteToken,
  			success: function(data) {
  				console.log(data)
  			}
  		})
  	}
  })

  jQuery(document).on('click', '#remove-photo', function(){
  	if( typeof deleteToken != 'undefined' ) {
  		jQuery.ajax({
  			type: 'POST',
  			url: 'https://api.cloudinary.com/v1_1/musemarketing/delete_by_token',
  			data: 'token=' + deleteToken,
  			success: function(data) {
  				console.log(data)
  			}
  		})
  	}
  })
</script>
