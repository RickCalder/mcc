
var map
var locations
var post
var name
var image
var newLocations

(function($) {
  if(window.location.pathname.indexOf('safety-campaign') >= 0 || window.location.pathname.indexOf('campagne-de-securite') >= 0 ) {
  } else {
    return false;
  }

  $('#remove-photo').on('click', function() {
    $('#image').val('')
    $("#photo-taken").attr('src', '')
    $('#take-photo').val('Add Photo')
    $(this).slideUp()
  })


  $("#leading-city").text(leaders.city);
  $("#leading-count").text(leaders.count);
  locations = newLocations;
  console.log(newLocations)
  $(document).on('click','.pledge-row .nectar-button', function(e) {
    e.preventDefault()
    window.scrollTo(0,0)
    $('.campaign-modal-overlay').fadeIn()
    $('.campaign-modal').fadeIn()
  })

  $(document).on('click','.campaign-modal-overlay', function(e) {
    e.preventDefault()
    $('.campaign-modal-overlay').fadeOut()
    $('.campaign-modal').fadeOut()
  })

  $('.campaign-form').on('submit', function(e) {
    e.preventDefault();

    $("#submit-campaign").val('Working....').prop('disabled', true)

    var errors = ''
    if(!$('.campaign-form input[name=pledge_type]:checked').length) {
      errors += '<p>' + MyAjax.translations.optionError + '</p>'
    }
    if($('#email-confirm').attr('checked') && $('#email').val() =='') {
      errors += '<p>' + MyAjax.translations.newsletterError + '</p>'
    } 
    if( $('#name').val()==='') {
      errors += '<p>' + MyAjax.translations.nameError + '</p>'
    }
    
    if( $('#city').val()==='') {
      errors += '<p>' + MyAjax.translations.cityError + '</p>'
    }
    
    if( $('#postal_code').val()==='') {
      errors += '<p>' + MyAjax.translations.postalError + '</p>'
    }
    if( errors !== '' ) {
      var html = '<h5>' + MyAjax.translations.errorsText + '</h5>' + errors
      $('.errors').html(html).slideDown()
        $("#submit-campaign").val(MyAjax.translations.pledgeButtonText).prop('disabled', false)
    } else {
      $('.errors').slideUp()
      var address = $('#postal_code').val()
      getCoords(address, function(err, coords) {
      if (err) {
        var html = '<h5>Please correct the following errors</h5><p>T' + MyAjax.translations.invalidPC + '</p></h5>'
        $('.errors').html(html).slideDown()
        $("#submit-campaign").val(MyAjax.translations.pledgeButtonText).prop('disabled', false)
        return  false
      }

      var newDate = new Date();
      var day = newDate.getDate();
      var months = [MyAjax.translations.jan, MyAjax.translations.feb, MyAjax.translations.mar, MyAjax.translations.apr, MyAjax.translations.may, MyAjax.translations.jun, MyAjax.translations.jul, MyAjax.translations.aug, MyAjax.translations.sep, MyAjax.translations.oct, MyAjax.translations.nov, MyAjax.translations.dec]

      var month = months[newDate.getMonth()]
      var today = month + ' ' + day + ', ' + newDate.getFullYear()


      if( typeof image != 'undefined') {
      var contentString = '<div class="info-window"><div class="info-image"><img src="' +  location['image'] + '" alt="' + MyAjax.translations.imageFor + location['name'] +'"></div><div class="info-content"><p class="map-name"><strong>' + location['name'] + ' </strong>' + MyAjax.translations.pledgeOn + ' ' + location['date'] + '.</p></div></div>'
      } else {
        var contentString = '<div class="info-window"><div class="info-content"><p class="map-name"><strong>' + $('#name').val() + ' </strong>' + MyAjax.translations.pledgeOn + '</strong> ' + today + '.</p></div></div>'
      }
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      })
       marker = new google.maps.Marker({
          position: coords,
          icon: '/wp-content/themes/mcc/img/icons/motorcycle.png',
       });
      marker.addListener('click', function() {
        infowindow.close()
        infowindow.open(map, marker);
      });
      marker.setMap(map);

       //Send off to save form data

        var data = {
          action: 'save_pledge',
          security : MyAjax.security,
          formdata : $("#campaign-form").serialize()
        };

        $.post(MyAjax.ajaxurl, data, function(response) {
          if( response ) {
            var hasImage = $('#image').val() == '' ? 'no' : 'yes'
            var pledger =$('#name').val()
            $('.campaign-modal-overlay').fadeOut()
            $('.campaign-modal').fadeOut()
            name = $('#name').val()
            $('.campaign-form')[0].reset();
            $('.pledge-row .nectar-button').css('display', 'none')
            $('.pledged h2').text(MyAjax.translations.pledgeThank + ' ' + pledger)
            $('.pledged').css('display', 'block')
            $('.no-pledge').css('display', 'none')
            if( hasImage == 'no') {
              $("html, body").animate({
                scrollTop: $('#tell').offset().top
              }, 500);
            } else {
              window.location.href = response
            }
          }
        });

    })
    }

  })



})(jQuery);


function getCoords(address, next) {
  return geocode(address, function(err, result) {
    if (err) {
      // Execute the callback with the err
      return next(err)
    }
    
    var bounds = result.geometry.bounds
    if(typeof bounds == 'undefined') {
      var lat = result.geometry.location.lat()
      var lng = result.geometry.location.lng()
    } else {
      var lat = bounds.f.b
      var lng = bounds.b.b
    }
       jQuery("#city").val(result.address_components[2].long_name)
       jQuery("#latitude").val(lat)
       jQuery("#longitude").val(lng)
    // Execute the callback with the result
    next(undefined, {
      lat: lat,
      lng: lng
    })
  })  
}

function geocode(address, next) {
  var geocoder = new google.maps.Geocoder()
  return geocoder.geocode({ address: address }, function(results, status) {
    if (status !== google.maps.GeocoderStatus.OK) {
      return next('Error geocoding address')
    }
    var match = findFirstMatchInCanada(results)
    if (!match) {
      return next('No result found in Canada')
    }
    return next(undefined, match)
  })
}

function findFirstMatchInCanada(results) {
  if (!results || !results.length) {
    return undefined
  }
  
  var match
  for (var i = 0; i < results.length; i++) {
    var result = results[i]
    if (isInCountry(result, 'Canada')) {
      match = result
      break
    }
  }
  return match
}

function isInCountry(result, country) {
  if( result.address_components.length > 5 ) {
     return result
       && result.address_components
       && result.address_components.length
       && result.address_components.length > 5
       && typeof result.address_components[5].long_name === 'string'
       && result.address_components[5].long_name.toLowerCase() === country.toLowerCase()
    } else {
     return result
       && result.address_components
       && result.address_components.length
       && result.address_components.length <= 5
       && typeof result.address_components[4].long_name === 'string'
       && result.address_components[4].long_name.toLowerCase() === country.toLowerCase()
    }
}

function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: {lat: 58.153678, lng: -105.465493}
  });

  // Create an array of alphabetical characters used to label the markers.

  var markers = locations.map(function(location, i) {
    if( location['image'] != '' ) {
      var contentString = '<div class="info-window"><div class="info-image"><img src="' +  location['image'] + '" alt="' + MyAjax.translations.imageFor + location['name'] +'"></div><div class="info-content"><p class="map-name"><strong>' + location['name'] + ' </strong>' + MyAjax.translations.pledgeOn + ' ' + location['date'] + '.</p></div></div>'
    } else {
      var contentString = '<div class="info-window"><p class="map-name"><strong>' + location['name'] + ' </strong>' + MyAjax.translations.pledgeOn + ' ' + location['date'] + '.</p></div>'
    }
    
    var coords = new google.maps.LatLng(location['lat'], location['lng']);
    var infowindow = new google.maps.InfoWindow({
      content: contentString
    })

    var marker = new google.maps.Marker({
      position: coords,
      icon: '../wp-content/themes/mcc/img/icons/motorcycle.png',
      title: location['name']
    });

    marker.addListener('mouseover', function() {
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
      infowindow.close(map, marker);
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });

    return marker
  });
 

  // Add a marker clusterer to manage the markers.
  var markerCluster = new MarkerClusterer(map, markers,
      {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

}




