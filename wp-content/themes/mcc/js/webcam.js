  (function() {
    'use strict'

    var video = document.querySelector('video')
      , canvas

    /**
     *  generates a still frame image from the stream in the <video>
     *  appends the image to the <body>
     */
     function clearOnCancel() {
      jQuery('#photo-taken').attr('src', '');
      jQuery('.photo-container').css('display', 'none');
      jQuery('#take-photo').prop('disabled',false).removeClass('disabled')
     }

     function clearOnNotAllowed() {
      jQuery('#take-photo').css('display', 'none');
      jQuery('.photo-error').css('display', 'block');
      jQuery('.photo-container').css('display', 'none');
      jQuery('#photo-taken').attr('src', '');
      jQuery('#take-photo').prop('disabled',false).removeClass('disabled')
     }

    function takeSnapshot() {
      var img = jQuery("#photo-taken")
      var context
      var width = video.offsetWidth
        , height = video.offsetHeight

      canvas = canvas || document.createElement('canvas')
      canvas.width = width
      canvas.height = height

      context = canvas.getContext('2d')
      context.drawImage(video, 0, 0, width, height)

      jQuery('#photo-taken').attr('src', canvas.toDataURL('image/png'))
      jQuery('#photo-taken').css('display', 'block')
      jQuery('#retake-photo').prop('disabled', false).removeClass('disabled')
      jQuery('#capture-photo').prop('disabled',true).addClass('disabled')
      jQuery('.campaign-form video').css('display', 'none')
    }

    /**
     * Button Functions
    */

    jQuery(document).on('click', '#retake-photo', function(e) {
      jQuery('#photo-taken').css('display', 'none')
      jQuery('.campaign-form video').css('display', 'block')
      jQuery('#retake-photo').prop('disabled', true).addClass('disabled')
      jQuery('#capture-photo').prop('disabled',false).removeClass('disabled')
    })

    jQuery(document).on('click', '#capture-photo', function(e) {
      takeSnapshot()
    })

    jQuery(document).on('click', '#cancel-photo', function(e) {
      clearOnCancel()
    })

    jQuery(document).on('click', '#take-photo', function(e){
      jQuery('.campaign-form video').css('display', 'block')
      jQuery('.photo-container').slideDown()
      jQuery('#retake-photo').prop('disabled', true).addClass('disabled')
      jQuery('#capture-photo').prop('disabled',false).removeClass('disabled')
      jQuery(this).prop('disabled',true).addClass('disabled')

      // use MediaDevices API
      // docs: https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
      if (navigator.mediaDevices) {
        // access the web cam
        navigator.mediaDevices.getUserMedia({video: true})
        // permission granted:
          .then(function(stream) {
            video.srcObject = stream
          })
          // permission denied:
          .catch(function(error) {
            clearOnNotAllowed()
          })
      }
      
    })
  })()