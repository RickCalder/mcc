<?php

class vcSafetyCampaign extends WPBakeryShortCode {

    // Element Init
  function __construct() {
    add_action( 'init', array( $this, 'vc_safetycampaign_mapping' ) );
    add_shortcode( 'vc_safetycampaign', array( $this, 'vc_safetycampaign_html' ) );
  }

    // Element Mapping
  public function vc_safetycampaign_mapping() {

        // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
      return;
    }

    vc_map( 
      array(
        'name' => __('Safety Campaign', 'text-domain'),
        'base' => 'vc_safetycampaign',
        'description' => __('Safety Campaign Element', 'text-domain'), 
        'category' => __('MCC Custom Elements', 'text-domain'),   
        'icon' => '',            
        'params' => array(   

          array(
            'type' => 'textfield',
            'holder' => '',
            'class' => 'title-class',
            'heading' => __( 'Title', 'text-domain' ),
            'param_name' => 'title',
            'value' => __( '', 'text-domain' ),
            'description' => __( 'Title', 'text-domain' )
            ),                    

          ),
        )
      );                                

  }


    // Element HTML
  public function vc_safetycampaign_html( $atts ) {

        // Params extraction
    extract(
      shortcode_atts(
        array(
          'title'         => '',
          ), 
        $atts
        )
      );

        // Fill $html var with data
    $html = '
    <div class="vc-safetycampaign-wrap">
      <div class="vc-safetycampaign-details">
        <div id="map"></div>
      </div>
    </div>
    ';      

    return $html;

  }

} // End Element Class


// Element Class Init
new vcsafetycampaign();  