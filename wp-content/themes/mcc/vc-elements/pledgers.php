<?php

class vcPledgers extends WPBakeryShortCode {

    // Element Init
  function __construct() {
    add_action( 'init', array( $this, 'vc_pledgers_mapping' ) );
    add_shortcode( 'vc_pledgers', array( $this, 'vc_pledgers_html' ) );
  }

    // Element Mapping
  public function vc_pledgers_mapping() {

        // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
      return;
    }

    vc_map( 
      array(
        'name' => __('Pledgers', 'mcc-map'),
        'base' => 'vc_pledgers',
        'description' => __('Pledgers Element', 'mcc-map'), 
        'category' => __('MCC Custom Elements', 'mcc-map'),   
        'icon' => '',            
        'params' => array(   

          array(
            'type' => 'textfield',
            'holder' => '',
            'heading' => __( 'Number', 'mcc-map' ),
            'param_name' => 'number',
            'value' => __( '', 'mcc-map' ),
            'description' => __( 'Number of pledgers you\'d like to display', 'mcc-map' )
            ),                    

          ),
        )
      );                                

  }


    // Element HTML
  public function vc_pledgers_html( $atts ) {

        // Params extraction
    extract(
      shortcode_atts(
        array(
          'number'         => '',
          ), 
        $atts
        )
      );

    $pledgers = query_posts( array( 'post_type' => array('pledgers'), 'posts_per_page' => $number, 'meta_key' => '_thumbnail_id' ) );
        // Fill $html var with data

    $html = '<div class="vc-pledgers-wrap">';
    
    foreach( $pledgers as $pledger ) {
      $html .= '
        <div class="vc-pledgers-details">
          <div class="vc-pledger-image">
            <img src="' . get_field('image', $pledger->ID) . '">
          </div>
          <div class="vc-pledger-name">' . $pledger->post_title .'</div>
          <div class="vc-pledger-city">' . get_field('city', $pledger->ID) .'</div>
        </div>
      ';      
    }
    $html .= '</div>';

    return $html;

  }

} // End Element Class


// Element Class Init
new vcPledgers();  