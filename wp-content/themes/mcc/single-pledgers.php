<?php get_header(); ?>

<?php 

global $nectar_theme_skin, $options;

$bg = get_post_meta($post->ID, '_nectar_header_bg', true);
$bg_color = get_post_meta($post->ID, '_nectar_header_bg_color', true);
$fullscreen_header = (!empty($options['blog_header_type']) && $options['blog_header_type'] == 'fullscreen' && is_singular('post')) ? true : false;
$blog_header_type = (!empty($options['blog_header_type'])) ? $options['blog_header_type'] : 'default';
$fullscreen_class = ($fullscreen_header == true) ? "fullscreen-header full-width-content" : null;
$theme_skin = (!empty($options['theme-skin']) && $options['theme-skin'] == 'ascend') ? 'ascend' : 'default';
$hide_sidebar = (!empty($options['blog_hide_sidebar'])) ? $options['blog_hide_sidebar'] : '0'; 
$blog_type = $options['blog_type']; 

if(have_posts()) : while(have_posts()) : the_post();

	nectar_page_header($post->ID); 

endwhile; endif;
?>

<div class="container-wrap <?php echo ($fullscreen_header == true) ? 'fullscreen-blog-header': null; ?> <?php if($blog_type == 'std-blog-fullwidth' || $hide_sidebar == '1') echo 'no-sidebar'; ?>">

	<div class="container main-content">
		<div id="tell" data-midnight="light" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-section standard_section   " style="padding: 150px 331px; margin-left: -331px; visibility: visible;"><div class="row-bg-wrap instance-3"> <div class="row-bg  using-bg-color  " style="background-color: #dd3333; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 light left">
	<div class="vc_col-sm-12 wpb_column column_container vc_column_container col centered-text no-extra-padding instance-4" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				
	<div id="fws_5abab255af4d4" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   " style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg   " style=""></div> </div><div class="col span_12  left">
	<div class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding instance-5" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
		<div class="wpb_wrapper">
			
		</div> 
	</div>
	</div> 

	<div class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding instance-6" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
		<div class="wpb_wrapper">
			
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">

			<h2><?php echo __('Thank You', 'mcc-maps');?> <?php the_title() ;?> <?php echo __('for Taking the Pledge!', 'mcc-maps');?></h2>
			<div class="thank-you-image">
				<img src="<?php echo get_field('image');?>" alt="<?php the_title()?> <?php echo __('taking the Motorcycling Safety Pledge', 'mcc-maps');?>">
			</div>


		</div>
	</div>

		</div> 
	</div>
	</div> 

	<div class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding instance-7" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
		<div class="wpb_wrapper">
			
		</div> 
	</div>
	</div> 
</div></div><div class="divider-wrap"><div style="height: 30px;" class="divider"></div></div>
	<div id="fws_5abab255afc6c" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   " style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg   " style=""></div> </div><div class="col span_12  left">
	<div class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding instance-8" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
		<div class="wpb_wrapper">
			
	<div class="wpb_raw_code wpb_content_element wpb_raw_html">
		<div class="wpb_wrapper">
			<div id="share-buttons">
   
    <!-- Facebook -->

   <a class="social-share-link" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>" target="_blank">
        <img src="http://motorcycling.ca/wp-content/uploads/2017/03/facebook.png" alt="Facebook">
    </a>


     
    <!-- Twitter -->
    <a class="social-share-link" href="https://twitter.com/share?url=<?php the_permalink();?>&amp;text=Please join me in taking the Motorcycle Safety Pledge at &amp;hashtags=MotorcycleSafetyPledge" target="_blank">
        <img src="http://motorcycling.ca/wp-content/uploads/2017/03/twitter.png" alt="Twitter">
    </a>

    <!-- Linkedin -->
    <a class="social-share-link" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink();?>&amp;title=Motorcycle Safety Pledge&amp;summary=Take the Motorcycle Safety Pledge&amp;source=" target="_blank">
        <img src="http://www.motorcycling.ca/wp-content/uploads/2017/05/linkedin.png" alt="Linkedin">
    </a>


    <!-- Email -->
    <a href="mailto:?Subject=Motorcycle Safety Pledge&amp;Body=Please join me in taking the Motorcycle Safety Pledge at motorcycling.ca/safety-campaign">
        <img src="http://motorcycling.ca/wp-content/uploads/2017/03/mailicon.png" alt="Email">
    </a>
</div>
		</div>
	</div>

		</div> 
	</div>
	</div> 
</div></div>
			</div> 
		</div>
	</div> 
</div></div>
</div>

		
	</div><!--/container-->

</div><!--/container-wrap-->
	
<?php get_footer(); ?>