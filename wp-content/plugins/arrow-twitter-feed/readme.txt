=== Twitter Feed ===
Contributors: arrowplugins,mutiullah
Tags: twitter feed, twitter, tweets, Follow Twitter, mobile Twitter, hashtag, like button, reply, retweet, timeline, twitter handle, twitter stream, Custom Twitter Feed, responsive Twitter, Twitter wall, Twitter widget, beautiful Twitter, free Twitter feed, best Twitter feed
Requires at least: 4.0
Tested up to: 4.7
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Twitter Feed Display beautifully clean, customizable, and responsive twitter feeds from Twitter accounts or any hashtag.

== Description ==

Twitter feed plugin helps you to display Twitter feeds from any public Twitter account, in same way you can also show Twitter feed from any hashtag and display Twitter feed into your posts, pages, home or in widget. With Twitter feed plugin you can add multiple Twitter feeds into single page, post or widget using shortcodes.

= Features =
* **Super Simple to set up** 
* **Change Date Posted Language** Multiple Language Support( English, Dutch, German, Italian, Russian & more... )
* **6 Feed Templates** for your Twitter feed display 
* **3 Feed Styles** for your Twitter feed layout
* **Vertical Feed Style** to show your Twitter feed in a line
* **Blog Style Feed**
* **Masonry Style Feed** to show Twitter feed in Pinterest like style
* Create **Unlimited** number of Feeds
* Display feeds from **Public accounts** 
* Display feeds from any **hashtag** 
* Create **Multiple Column** Masonry Feed
* Select number of columns for Masonry Feed
* Limit Caption Text (number of characters of caption text to show)
* Show only photos in feed
* Show/Hide Display Picture
* Show/Hide Post Date
* Show/Hide Caption Text
* Completely **Responsive** and mobile ready - layout looks great on any screen size and in any container width
* Display **multiple Twitter feeds** on the same page or on different pages throughout your site
* Use the built-in **shortcode** 


== Installation ==

**The easy way** :

* Go to the Plugins Menu in WordPress.
* Search for plugin Twitter Feed ArrowPlugins"
* Click "Install".
* After Installation click activate to start using the Twitter Feed.
* Go to Twitter Feed from Dashboard menu.

Not so easy way :

* To install Twitter Feed via FTP
* Download the Twitter Feed
* Unarchive Twitter Feed plugin
* Copy folder with arrow-twitter-feed.zip
* Open the ftp \wp-content\plugins\
* Paste the plug-ins folder in the folder
* Go to admin panel => open item "Plugins" => activate Twitter feed.
* Go to Twitter feed from Dashboard menu.




== Screenshots ==

1. All Twitter Feeds
2. Twitter Feed Settings
3. Twitter Feed Templates
4. Twitter Feed Vertical View for Widgets
5. Twitter Feed Masonry
6. Twitter Feed Blog Style View


== Frequently Asked Questions ==
<strong>Q. Why do I need Twitter Feed Plugin?</strong>

A. Increase engagement between you and your users, customers, fans or group members. Get more likes by displaying your Twitter Page content directly on your site.

<strong>Q. Does the plugin update the feed in real time ?</strong>

A. Yes, the plugin update the feed in real time, and fetch the latest Twitter Tweets instantly.

<strong>Q. How to add new Twitter Feed to your WordPress site ?</strong>

A. After activating the Twitter Feed plugin, you'll see the Twitter Feed menu in WordPress menu, click on it and you'll see the Add New Twitter Feed button on the top of the Twitter Feeds Page. Choose your desired settings, and use the shortcode to show your Twitter Feed.

<strong>Q. What can Twitter Feed Plugin do ?</strong>

A. Display statuses, photos, links from your Twitter page. Display multiple feeds from different Twitter pages.

<strong>Q. Coding Required ?</strong>

A. No Coding Required, Just add the shortcode and you are good to go. Your Twitter Feed instantly Show up on your Site.

<strong>Q. How to show Twitter Feed in Post/Page ?</strong>

A. To show your Twitter Feed in your Post/Page, copy the provided shortcode from Twitter Feed settings page into your Post where you want to show your Twitter Feed.

<strong>Q. How to show Twitter Feed in your Widget Area ?</strong>

A. To show Twitter Feed in your Widget Area, go into your Widgets and find the Text Widget, drage the Text Widget into your Sidebar area and paste the provided shortcode from your Twitter Feed settings page.



== Changelog ==
= 1.1 =

* Fixed feed libraray
* Removed the conflict with other plugins
* 
Fixed shortcode problem, where in FireFox shortcode doesn't show up in admin page
* 
Fixed the problem while some users having issues with uninstalling the plugin
* 
Few bug fixes
Some admin changes

= 1.0 =
* Launched the Twitter Feed plugin!
